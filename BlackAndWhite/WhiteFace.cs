﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackAndWhite
{
    class WhiteFace:Face
    {
        public WhiteFace(int x, int y, int hp) : base(x, y, (char)2, hp)
        {
        }
        
        public void Eat(BlackFace face)
        {
            hp += face.HP;
        }

        public void Move(ConsoleKey key, int xMin, int xMax, int yMin, int yMax)
        {
            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    if (x > xMin)
                    {
                        x--;
                    }
                    break;
                case ConsoleKey.RightArrow:
                    if (x < xMax)
                    {
                        x++;
                    }
                    break;
                case ConsoleKey.UpArrow:
                    if (y > yMin)
                    {
                        y--;
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (y < yMax)
                    {
                        y++;
                    }
                    break;
            }
        }
    }
}
