﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackAndWhite
{
    class Face
    {
        protected int x, y;
        private char look;
        protected int hp;

        protected Face(int x, int y, char look, int hp)
        {
            this.x = x;
            this.y = y;
            this.look = look;
            this.hp = hp;
        }

        public int HP
        {
            get { return hp; }
        }

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public void Draw()
        {
            Console.SetCursorPosition(x,y);
            Console.Write(look);
        }

        public bool IsCollisionWithAnother(Face face)
        {
            return x == face.x && y == face.y;
        }
    }
}
