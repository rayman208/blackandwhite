﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackAndWhite
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager game = new GameManager(10);
            game.Play();

            Console.ReadKey();
        }
    }
}
